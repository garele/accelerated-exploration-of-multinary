#ALE 

This Pyhton module allows to plot Accumulated Local Effect of IA models trained by PyTerK. 

##Requirements:
* Install following libraries, via pip or conda
	* pandas 
	* PyALE
* Create environment variables:
	* path to folder that contains the training results : DATASETS_DIR=$path/to/datasets/ 
	* path to folder that contains the training results : export RUN_DIR= $path/to/run/

