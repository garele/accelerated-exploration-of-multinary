# ------------------------------------------------------------------
#                  ____        _            _
#                 |  _ \ _   _| |_ ___ _ __| | __
#                 | |_) | | | | __/ _ \ '__| |/ /
#                 |  __/| |_| | ||  __/ |  |   <
#                 |_|    \__, |\__\___|_|  |_|\_\
#                        |___/                        Settings file
# ------------------------------------------------------------------
# Python Iterator for Kfold and co. - pjluc 2021
#
# This is a sample configuration
# 
# Note : Afin d'être portable, le paramètre datasets_dir peut être
#        forcé via une variable d'environnement.

global:
    description: Train models by adding binaries one by one.
    settings v.: 1.0
    seed: 123
    datasets_dir: ./BINAIRES/
    run_dir: ./campaign15/


datasets:
    
    compo_E_add_1binary:
        filename: Compo_E_wo_outlier_1_binaire.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['E (GPa)']

    compo_E_add_2binaries:
        filename: Compo_E_wo_outlier_2_binaires.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['E (GPa)']
    

    compo_H_add_1binary:
        filename: Compo_H_wo_outlier_1_binaire.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['H (GPa)']
        
    compo_H_add_2binaries:
        filename: Compo_H_wo_outlier_2_binaires.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['H (GPa)']

    
    compo_E_add_1binary_averaged:
        filename: Data_averaged_1_binaire.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['E (GPa)'] 
    
    compo_E_add_2binaries_averaged: 
        filename: Data_averaged_2_binaires.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['E (GPa)'] 
    
    
    compo_H_add_1binary_averaged:
        filename: Data_averaged_1_binaire.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['H (GPa)'] 

    compo_H_add_2binaries_averaged:
        filename: Data_averaged_2_binaires.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['H (GPa)'] 



    compo_XRD_class_add_1binary:
        filename: Database_XRD_1_binaire.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['XRD (0: amorphous)']

    compo_XRD_class_add_2binaries:
        filename: Database_XRD_2_binaires.csv
        columns_x: ['Zr_at','Nb_at','Mo_at','Ti_at','Cr_at']
        columns_y: ['XRD (0: amorphous)']


   


models:

    # RF fit E
    sklearn-RF-100-10:
        module: sklearn.ensemble
        class: RandomForestRegressor
        args: { 'n_estimators':100 , 'min_samples_split':10}

    # RF fit H     
    sklearn-RF-150-10:
        module: sklearn.ensemble
        class: RandomForestRegressor
        args: { 'n_estimators':150 ,  'criterion':'mae', 'min_samples_split':10}


    #RF_moy fit E
    sklearn-RF-65-2:
        module: sklearn.ensemble
        class: RandomForestRegressor
        args: { 'n_estimators':65 ,  'criterion':'mae', 'min_samples_split':2}

    #RF_moy fit H
    sklearn-RF-55-2:
        module: sklearn.ensemble
        class: RandomForestRegressor
        args: { 'n_estimators':55 ,  'criterion':'mae', 'min_samples_split':2}


    # NN fit E & H , NN moy fit E et H
    keras-100x100x100x100:
        module: my_models
        function: get_keras_mpp
        args: { 'input_shape':[5], 'neurons':[100,100,100,100,1], 'activations':['relu','relu','relu','relu',None], 'optimizer':'adam', 'loss':'mse', 'metrics':['mse','mae','mape'] }

    # class XRD
    keras-50x50x100x100:
        module: my_models
        function: get_keras_mpp
        args: { 'input_shape':[5], 'neurons':[50,50,100,100,1], 'activations':['relu','relu','relu','relu','sigmoid'], 'optimizer':'adam', 'loss':'binary_crossentropy', 'metrics':['accuracy','binary_crossentropy'] }
    


runs:

   

        

    Fit_E_non_averaged_data:
        description: Prediction of E on non averaged datasets
        datasets: [ 'compo_E_add_1binary', 'compo_E_add_2binaries']
        models: [ 'sklearn-RF-100-10', 'keras-100x100x100x100' ]
        n_iters: [ 30 ]
        k_folds: [ 5 ]
        epochs: [ 20 ]
        batch_sizes: [ 8 ]
        seed: 123
        save_yytest: True 

    Fit_H_non_averaged_data:
        description: Prediction of H on non averaged datasets
        datasets: [ 'compo_H_add_1binary', 'compo_H_add_2binaries']
        models: [ 'sklearn-RF-150-10', 'keras-100x100x100x100']
        n_iters: [ 30 ]
        k_folds: [ 5 ]
        epochs: [ 200 ]
        batch_sizes: [ 8 ]
        seed: 123
        save_yytest: True 
    
    Fit_E_averaged_data:
        description: Prediction of E on averaged datasets
        datasets: [ 'compo_E_add_1binary_averaged', 'compo_E_add_2binaries_averaged']
        models: [ 'sklearn-RF-65-2', 'keras-100x100x100x100' ]
        n_iters: [ 30 ]
        k_folds: [ 5 ]
        epochs: [ 200 ]
        batch_sizes: [ 8 ]
        seed: 123
        save_yytest: True 

    Fit_H_averaged_data:
        description: Prediction of H on averaged datasets
        datasets: ['compo_H_add_1binary_averaged', 'compo_H_add_2binaries_averaged']
        models: [ 'sklearn-RF-55-2', 'keras-100x100x100x100' ]
        n_iters: [ 30 ]
        k_folds: [ 5 ]
        epochs: [ 200 ]
        batch_sizes: [ 8 ]
        seed: 123
        save_yytest: True 