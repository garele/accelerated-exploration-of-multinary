close all
clear

data=xlsread('/Users/elisegarel/Desktop/THESE/BASE DE DONNEES/Nanoindentation/Python_extrac/Raw_data_corrected.xlsx');


compo_at=data(:,1:5)/100;
compo_m=data(:,6:10)/100;
E=data(:,11);
H=data(:,12);


figure;
subplot(2,3,1)
plot(compo_at(:,1),E,'+')
title('E as a function of Zr composition')
subplot(2,3,2)
plot(compo_at(:,2),E,'+')
title('E as a function of Nb composition')
subplot(2,3,3)
plot(compo_at(:,3),E,'+')
title('E as a function of Mo composition')
subplot(2,3,4)
plot(compo_at(:,4),E,'+')
title('E as a function of Ti composition')
subplot(2,3,5)
plot(compo_at(:,5),E,'+')
title('E as a function of Cr composition')

figure;
subplot(2,3,1)
plot(compo_at(:,1),H,'+')
title('H as a function of Zr composition')
subplot(2,3,2)
plot(compo_at(:,2),H,'+')
title('H as a function of Nb composition')
subplot(2,3,3)
plot(compo_at(:,3),H,'+')
title('H as a function of Mo composition')
subplot(2,3,4)
plot(compo_at(:,4),H,'+')
title('H as a function of Ti composition')
subplot(2,3,5)
plot(compo_at(:,5),H,'+')
title('H as a function of Cr composition')

b =10.*ones(31,1);
modelfun=@(b,x)b(1).*x(:,1)+b(2).*x(:,2)+b(3).*x(:,3)+b(4).*x(:,4)+b(5).*x(:,5)+b(6).*x(:,1).*x(:,2)+b(7).*x(:,1).*x(:,3)+b(8).*x(:,1).*x(:,4)+b(9).*x(:,1).*x(:,5)+b(10).*x(:,2).*x(:,3)+b(11).*x(:,2).*x(:,4)+b(12).*x(:,2).*x(:,5)+b(13).*x(:,3).*x(:,4)+b(14).*x(:,3).*x(:,5)+b(15).*x(:,4).*x(:,5)+b(16).*x(:,1).*x(:,2).*x(:,3)+b(17).*x(:,1).*x(:,2).*x(:,4)+b(18).*x(:,1).*x(:,2).*x(:,5)+b(19).*x(:,1).*x(:,3).*x(:,4)+b(20).*x(:,1).*x(:,3).*x(:,5)+b(21).*x(:,1).*x(:,4).*x(:,5)+b(22).*x(:,2).*x(:,3).*x(:,4)+b(23).*x(:,2).*x(:,3).*x(:,5)+b(24).*x(:,2).*x(:,4).*x(:,5)+b(25).*x(:,3).*x(:,4).*x(:,5)+b(26).*x(:,1).*x(:,2).*x(:,3).*x(:,4)+b(27).*x(:,1).*x(:,2).*x(:,3).*x(:,5)+b(28).*x(:,1).*x(:,2).*x(:,4).*x(:,5)+b(29).*x(:,1).*x(:,3).*x(:,4).*x(:,5)+b(30).*x(:,2).*x(:,3).*x(:,4).*x(:,5)+b(31).*x(:,1).*x(:,2).*x(:,3).*x(:,4).*x(:,5);

modelE=fitnlm(compo_at,E,modelfun,b)
resultsE=table2array(modelE.Coefficients);
coeffE=resultsE(:,1);
E_model=coeffE(1).*compo_at(:,1)+coeffE(2).*compo_at(:,2)+coeffE(3).*compo_at(:,3)+coeffE(4).*compo_at(:,4)+coeffE(5).*compo_at(:,5)+coeffE(6).*compo_at(:,1).*compo_at(:,2)+coeffE(7).*compo_at(:,1).*compo_at(:,3)+coeffE(8).*compo_at(:,1).*compo_at(:,4)+coeffE(9).*compo_at(:,1).*compo_at(:,5)+coeffE(10).*compo_at(:,2).*compo_at(:,3)+coeffE(11).*compo_at(:,2).*compo_at(:,4)+coeffE(12).*compo_at(:,2).*compo_at(:,5)+coeffE(13).*compo_at(:,3).*compo_at(:,4)+coeffE(14).*compo_at(:,3).*compo_at(:,5)+coeffE(15).*compo_at(:,4).*compo_at(:,5)+coeffE(16).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3)+coeffE(17).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4)+coeffE(18).*compo_at(:,1).*compo_at(:,2).*compo_at(:,5)+coeffE(19).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4)+coeffE(20).*compo_at(:,1).*compo_at(:,3).*compo_at(:,5)+coeffE(21).*compo_at(:,1).*compo_at(:,4).*compo_at(:,5)+coeffE(22).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffE(23).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffE(24).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffE(25).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE(26).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffE(27).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffE(28).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffE(29).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE(30).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE(31).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5);
figure;
plot(E, E_model,'+')
xlabel('Experimental E value')
ylabel('Modeled E value')
title('Elastic modulus (GPa) exeprimentally measured vs modeled')

b =10.*zeros(31,1);
modelfun_signif=@(b,x)b(1).*x(:,1)+b(2).*x(:,2)+b(3).*x(:,3)+b(4).*x(:,4)+b(5).*x(:,5)+0.*x(:,1).*x(:,2)+b(7).*x(:,1).*x(:,3)+b(8).*x(:,1).*x(:,4)+b(9).*x(:,1).*x(:,5)+0.*x(:,2).*x(:,3)+b(11).*x(:,2).*x(:,4)+b(12).*x(:,2).*x(:,5)+b(13).*x(:,3).*x(:,4)+0.*x(:,3).*x(:,5)+b(15).*x(:,4).*x(:,5)+b(16).*x(:,1).*x(:,2).*x(:,3)+b(17).*x(:,1).*x(:,2).*x(:,4)+0.*x(:,1).*x(:,2).*x(:,5)+b(19).*x(:,1).*x(:,3).*x(:,4)+0.*x(:,1).*x(:,3).*x(:,5)+0.*x(:,1).*x(:,4).*x(:,5)+b(22).*x(:,2).*x(:,3).*x(:,4)+b(23).*x(:,2).*x(:,3).*x(:,5)+0.*x(:,2).*x(:,4).*x(:,5)+b(25).*x(:,3).*x(:,4).*x(:,5)+0.*x(:,1).*x(:,2).*x(:,3).*x(:,4)+b(27).*x(:,1).*x(:,2).*x(:,3).*x(:,5)+0.*x(:,1).*x(:,2).*x(:,4).*x(:,5)+b(29).*x(:,1).*x(:,3).*x(:,4).*x(:,5)+b(30).*x(:,2).*x(:,3).*x(:,4).*x(:,5)+0.*x(:,1).*x(:,2).*x(:,3).*x(:,4).*x(:,5);
modelE_raffined=fitnlm(compo_at,E,modelfun_signif,b)
resultsE_raffined=table2array(modelE_raffined.Coefficients);
coeffE_raffined=resultsE_raffined(:,1);
E_model_raffined=coeffE_raffined(1).*compo_at(:,1)+coeffE_raffined(2).*compo_at(:,2)+coeffE_raffined(3).*compo_at(:,3)+coeffE_raffined(4).*compo_at(:,4)+coeffE_raffined(5).*compo_at(:,5)+coeffE_raffined(6).*compo_at(:,1).*compo_at(:,2)+coeffE_raffined(7).*compo_at(:,1).*compo_at(:,3)+coeffE_raffined(8).*compo_at(:,1).*compo_at(:,4)+coeffE_raffined(9).*compo_at(:,1).*compo_at(:,5)+coeffE_raffined(10).*compo_at(:,2).*compo_at(:,3)+coeffE_raffined(11).*compo_at(:,2).*compo_at(:,4)+coeffE_raffined(12).*compo_at(:,2).*compo_at(:,5)+coeffE_raffined(13).*compo_at(:,3).*compo_at(:,4)+coeffE_raffined(14).*compo_at(:,3).*compo_at(:,5)+coeffE_raffined(15).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(16).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3)+coeffE_raffined(17).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4)+coeffE_raffined(18).*compo_at(:,1).*compo_at(:,2).*compo_at(:,5)+coeffE_raffined(19).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4)+coeffE_raffined(20).*compo_at(:,1).*compo_at(:,3).*compo_at(:,5)+coeffE_raffined(21).*compo_at(:,1).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(22).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffE_raffined(23).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffE_raffined(24).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(25).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(26).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffE_raffined(27).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffE_raffined(28).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(29).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(30).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffE_raffined(31).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5);
figure;
plot(E, E_model_raffined,'+')
xlabel('Experimental E value')
ylabel('Modeled E value')
title('Elastic modulus (GPa) exeprimentally measured vs raffined modeled')



b =10.*ones(31,1);
modelfun=@(b,x)b(1).*x(:,1)+b(2).*x(:,2)+b(3).*x(:,3)+b(4).*x(:,4)+b(5).*x(:,5)+b(6).*x(:,1).*x(:,2)+b(7).*x(:,1).*x(:,3)+b(8).*x(:,1).*x(:,4)+b(9).*x(:,1).*x(:,5)+b(10).*x(:,2).*x(:,3)+b(11).*x(:,2).*x(:,4)+b(12).*x(:,2).*x(:,5)+b(13).*x(:,3).*x(:,4)+b(14).*x(:,3).*x(:,5)+b(15).*x(:,4).*x(:,5)+b(16).*x(:,1).*x(:,2).*x(:,3)+b(17).*x(:,1).*x(:,2).*x(:,4)+b(18).*x(:,1).*x(:,2).*x(:,5)+b(19).*x(:,1).*x(:,3).*x(:,4)+b(20).*x(:,1).*x(:,3).*x(:,5)+b(21).*x(:,1).*x(:,4).*x(:,5)+b(22).*x(:,2).*x(:,3).*x(:,4)+b(23).*x(:,2).*x(:,3).*x(:,5)+b(24).*x(:,2).*x(:,4).*x(:,5)+b(25).*x(:,3).*x(:,4).*x(:,5)+b(26).*x(:,1).*x(:,2).*x(:,3).*x(:,4)+b(27).*x(:,1).*x(:,2).*x(:,3).*x(:,5)+b(28).*x(:,1).*x(:,2).*x(:,4).*x(:,5)+b(29).*x(:,1).*x(:,3).*x(:,4).*x(:,5)+b(30).*x(:,2).*x(:,3).*x(:,4).*x(:,5)+b(31).*x(:,1).*x(:,2).*x(:,3).*x(:,4).*x(:,5);
modelH=fitnlm(compo_at,H,modelfun,b)
resultsH=table2array(modelH.Coefficients);
coeffH=resultsH(:,1);
H_model=coeffH(1).*compo_at(:,1)+coeffH(2).*compo_at(:,2)+coeffH(3).*compo_at(:,3)+coeffH(4).*compo_at(:,4)+coeffH(5).*compo_at(:,5)+coeffH(6).*compo_at(:,1).*compo_at(:,2)+coeffH(7).*compo_at(:,1).*compo_at(:,3)+coeffH(8).*compo_at(:,1).*compo_at(:,4)+coeffH(9).*compo_at(:,1).*compo_at(:,5)+coeffH(10).*compo_at(:,2).*compo_at(:,3)+coeffH(11).*compo_at(:,2).*compo_at(:,4)+coeffH(12).*compo_at(:,2).*compo_at(:,5)+coeffH(13).*compo_at(:,3).*compo_at(:,4)+coeffH(14).*compo_at(:,3).*compo_at(:,5)+coeffH(15).*compo_at(:,4).*compo_at(:,5)+coeffH(16).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3)+coeffH(17).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4)+coeffH(18).*compo_at(:,1).*compo_at(:,2).*compo_at(:,5)+coeffH(19).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4)+coeffH(20).*compo_at(:,1).*compo_at(:,3).*compo_at(:,5)+coeffH(21).*compo_at(:,1).*compo_at(:,4).*compo_at(:,5)+coeffH(22).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffH(23).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffH(24).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffH(25).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH(26).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffH(27).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffH(28).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffH(29).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH(30).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH(31).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5);
figure;
plot(H, H_model,'+')
xlabel('Experimental H value')
ylabel('Modeled H value')
title('Hardness (GPa) exeprimentally measured vs modeled')

b =10.*zeros(31,1);
modelfun_raffined=@(b,x)0.*x(:,1)+b(2).*x(:,2)+b(3).*x(:,3)+b(4).*x(:,4)+b(5).*x(:,5)+b(6).*x(:,1).*x(:,2)+b(7).*x(:,1).*x(:,3)+b(8).*x(:,1).*x(:,4)+b(9).*x(:,1).*x(:,5)+b(10).*x(:,2).*x(:,3)+b(11).*x(:,2).*x(:,4)+b(12).*x(:,2).*x(:,5)+b(13).*x(:,3).*x(:,4)+b(14).*x(:,3).*x(:,5)+0.*x(:,4).*x(:,5)+b(16).*x(:,1).*x(:,2).*x(:,3)+b(17).*x(:,1).*x(:,2).*x(:,4)+b(18).*x(:,1).*x(:,2).*x(:,5)+b(19).*x(:,1).*x(:,3).*x(:,4)+b(20).*x(:,1).*x(:,3).*x(:,5)+0.*x(:,1).*x(:,4).*x(:,5)+b(22).*x(:,2).*x(:,3).*x(:,4)+b(23).*x(:,2).*x(:,3).*x(:,5)+0.*x(:,2).*x(:,4).*x(:,5)+b(25).*x(:,3).*x(:,4).*x(:,5)+b(26).*x(:,1).*x(:,2).*x(:,3).*x(:,4)+0.*x(:,1).*x(:,2).*x(:,3).*x(:,5)+0.*x(:,1).*x(:,2).*x(:,4).*x(:,5)+b(29).*x(:,1).*x(:,3).*x(:,4).*x(:,5)+0.*x(:,2).*x(:,3).*x(:,4).*x(:,5)+0.*x(:,1).*x(:,2).*x(:,3).*x(:,4).*x(:,5);
modelH_raffined=fitnlm(compo_at,H,modelfun_raffined,b)
resultsH_raffined=table2array(modelH_raffined.Coefficients);
coeffH_raffined=resultsH_raffined(:,1);
H_model_raffined=coeffH_raffined(1).*compo_at(:,1)+coeffH_raffined(2).*compo_at(:,2)+coeffH_raffined(3).*compo_at(:,3)+coeffH_raffined(4).*compo_at(:,4)+coeffH_raffined(5).*compo_at(:,5)+coeffH_raffined(6).*compo_at(:,1).*compo_at(:,2)+coeffH_raffined(7).*compo_at(:,1).*compo_at(:,3)+coeffH_raffined(8).*compo_at(:,1).*compo_at(:,4)+coeffH_raffined(9).*compo_at(:,1).*compo_at(:,5)+coeffH_raffined(10).*compo_at(:,2).*compo_at(:,3)+coeffH_raffined(11).*compo_at(:,2).*compo_at(:,4)+coeffH_raffined(12).*compo_at(:,2).*compo_at(:,5)+coeffH_raffined(13).*compo_at(:,3).*compo_at(:,4)+coeffH_raffined(14).*compo_at(:,3).*compo_at(:,5)+coeffH_raffined(15).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(16).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3)+coeffH_raffined(17).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4)+coeffH_raffined(18).*compo_at(:,1).*compo_at(:,2).*compo_at(:,5)+coeffH_raffined(19).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4)+coeffH_raffined(20).*compo_at(:,1).*compo_at(:,3).*compo_at(:,5)+coeffH_raffined(21).*compo_at(:,1).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(22).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffH_raffined(23).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffH_raffined(24).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(25).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(26).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4)+coeffH_raffined(27).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,5)+coeffH_raffined(28).*compo_at(:,1).*compo_at(:,2).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(29).*compo_at(:,1).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(30).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5)+coeffH_raffined(31).*compo_at(:,1).*compo_at(:,2).*compo_at(:,3).*compo_at(:,4).*compo_at(:,5);
figure;
plot(H, H_model_raffined,'+')
xlabel('Experimental H value')
ylabel('Modeled H value')
title('Hardness (GPa) exeprimentally measured vs raffined modeled')


figure;
hold on
bar (coeffE,'y')
text(1,0,'Zr','FontSize',10,'Rotation',-90*sign(coeffE(1)))
text(2,0,'Nb','FontSize',10,'Rotation',-90*sign(coeffE(2)))
text(3,0,'Mo','FontSize',10,'Rotation',-90*sign(coeffE(3)))
text(4,0,'Ti','FontSize',10,'Rotation',-90*sign(coeffE(4)))
text(5,0,'Cr','FontSize',10,'Rotation',-90*sign(coeffE(5)))
text(6,0,'ZrNb','FontSize',10,'Rotation',-90*sign(coeffE(6)))
text(7,0,'ZrMo','FontSize',10,'Rotation',-90*sign(coeffE(7)))
text(8,0,'ZrTi','FontSize',10,'Rotation',-90*sign(coeffE(8)))
text(9,0,'ZrCr','FontSize',10,'Rotation',-90*sign(coeffE(9)))
text(10,0,'NbMo','FontSize',10,'Rotation',-90*sign(coeffE(10)))
text(11,0,'NbTi','FontSize',10,'Rotation',-90*sign(coeffE(11)))
text(12,0,'NbCr','FontSize',10,'Rotation',-90*sign(coeffE(12)))
text(13,0,'MoTi','FontSize',10,'Rotation',-90*sign(coeffE(13)))
text(14,0,'MoCr','FontSize',10,'Rotation',-90*sign(coeffE(14)))
text(15,0,'TiCr','FontSize',10,'Rotation',-90*sign(coeffE(15)))
text(16,0,'ZrNbMo','FontSize',10,'Rotation',-90*sign(coeffE(16)))
text(17,0,'ZrNbTi','FontSize',10,'Rotation',-90*sign(coeffE(17)))
text(18,0,'ZrNbCr','FontSize',10,'Rotation',-90*sign(coeffE(18)))
text(19,0,'ZrMoTi','FontSize',10,'Rotation',-90*sign(coeffE(19)))
text(20,0,'ZrMoCr','FontSize',10,'Rotation',-90*sign(coeffE(20)))
text(21,0,'ZrTiCr','FontSize',10,'Rotation',-90*sign(coeffE(21)))
text(22,0,'NbTiMo','FontSize',10,'Rotation',-90*sign(coeffE(22)))
text(23,0,'NbTiCr','FontSize',10,'Rotation',-90*sign(coeffE(23)))
text(24,0,'NbMoCr','FontSize',10,'Rotation',-90*sign(coeffE(24)))
text(25,0,'TiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(25)))
text(26,0,'ZrNbTiMo','FontSize',10,'Rotation',-90*sign(coeffE(26)))
text(27,0,'ZrNbTiCr','FontSize',10,'Rotation',-90*sign(coeffE(27)))
text(28,0,'ZrNbMoCr','FontSize',10,'Rotation',-90*sign(coeffE(28)))
text(29,0,'ZrTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(29)))
text(30,0,'NbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(30)))
text(31,0,'ZrNbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(31)))
hold off


figure;
hold on
bar (coeffE_raffined,'y')
text(1,0,'Zr','FontSize',10,'Rotation',-90*sign(coeffE(1)))
text(2,0,'Nb','FontSize',10,'Rotation',-90*sign(coeffE(2)))
text(3,0,'Mo','FontSize',10,'Rotation',-90*sign(coeffE(3)))
text(4,0,'Ti','FontSize',10,'Rotation',-90*sign(coeffE(4)))
text(5,0,'Cr','FontSize',10,'Rotation',-90*sign(coeffE(5)))
text(6,0,'ZrNb','FontSize',10,'Rotation',-90*sign(coeffE(6)))
text(7,0,'ZrMo','FontSize',10,'Rotation',-90*sign(coeffE(7)))
text(8,0,'ZrTi','FontSize',10,'Rotation',-90*sign(coeffE(8)))
text(9,0,'ZrCr','FontSize',10,'Rotation',-90*sign(coeffE(9)))
text(10,0,'NbMo','FontSize',10,'Rotation',-90*sign(coeffE(10)))
text(11,0,'NbTi','FontSize',10,'Rotation',-90*sign(coeffE(11)))
text(12,0,'NbCr','FontSize',10,'Rotation',-90*sign(coeffE(12)))
text(13,0,'MoTi','FontSize',10,'Rotation',-90*sign(coeffE(13)))
text(14,0,'MoCr','FontSize',10,'Rotation',-90*sign(coeffE(14)))
text(15,0,'TiCr','FontSize',10,'Rotation',-90*sign(coeffE(15)))
text(16,0,'ZrNbMo','FontSize',10,'Rotation',-90*sign(coeffE(16)))
text(17,0,'ZrNbTi','FontSize',10,'Rotation',-90*sign(coeffE(17)))
text(18,0,'ZrNbCr','FontSize',10,'Rotation',-90*sign(coeffE(18)))
text(19,0,'ZrMoTi','FontSize',10,'Rotation',-90*sign(coeffE(19)))
text(20,0,'ZrMoCr','FontSize',10,'Rotation',-90*sign(coeffE(20)))
text(21,0,'ZrTiCr','FontSize',10,'Rotation',-90*sign(coeffE(21)))
text(22,0,'NbTiMo','FontSize',10,'Rotation',-90*sign(coeffE(22)))
text(23,0,'NbTiCr','FontSize',10,'Rotation',-90*sign(coeffE(23)))
text(24,0,'NbMoCr','FontSize',10,'Rotation',-90*sign(coeffE(24)))
text(25,0,'TiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(25)))
text(26,0,'ZrNbTiMo','FontSize',10,'Rotation',-90*sign(coeffE(26)))
text(27,0,'ZrNbTiCr','FontSize',10,'Rotation',-90*sign(coeffE(27)))
text(28,0,'ZrNbMoCr','FontSize',10,'Rotation',-90*sign(coeffE(28)))
text(29,0,'ZrTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(29)))
text(30,0,'NbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(30)))
text(31,0,'ZrNbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffE(31)))
hold off


figure;
hold on
bar (coeffH,'y')
text(1,0,'Zr','FontSize',10,'Rotation',-90*sign(coeffH(1)))
text(2,0,'Nb','FontSize',10,'Rotation',-90*sign(coeffH(2)))
text(3,0,'Mo','FontSize',10,'Rotation',-90*sign(coeffH(3)))
text(4,0,'Ti','FontSize',10,'Rotation',-90*sign(coeffH(4)))
text(5,0,'Cr','FontSize',10,'Rotation',-90*sign(coeffH(5)))
text(6,0,'ZrNb','FontSize',10,'Rotation',-90*sign(coeffH(6)))
text(7,0,'ZrMo','FontSize',10,'Rotation',-90*sign(coeffH(7)))
text(8,0,'ZrTi','FontSize',10,'Rotation',-90*sign(coeffH(8)))
text(9,0,'ZrCr','FontSize',10,'Rotation',-90*sign(coeffH(9)))
text(10,0,'NbMo','FontSize',10,'Rotation',-90*sign(coeffH(10)))
text(11,0,'NbTi','FontSize',10,'Rotation',-90*sign(coeffH(11)))
text(12,0,'NbCr','FontSize',10,'Rotation',-90*sign(coeffH(12)))
text(13,0,'MoTi','FontSize',10,'Rotation',-90*sign(coeffH(13)))
text(14,0,'MoCr','FontSize',10,'Rotation',-90*sign(coeffH(14)))
text(15,0,'TiCr','FontSize',10,'Rotation',-90*sign(coeffH(15)))
text(16,0,'ZrNbMo','FontSize',10,'Rotation',-90*sign(coeffH(16)))
text(17,0,'ZrNbTi','FontSize',10,'Rotation',-90*sign(coeffH(17)))
text(18,0,'ZrNbCr','FontSize',10,'Rotation',-90*sign(coeffH(18)))
text(19,0,'ZrMoTi','FontSize',10,'Rotation',-90*sign(coeffH(19)))
text(20,0,'ZrMoCr','FontSize',10,'Rotation',-90*sign(coeffH(20)))
text(21,0,'ZrTiCr','FontSize',10,'Rotation',-90*sign(coeffH(21)))
text(22,0,'NbTiMo','FontSize',10,'Rotation',-90*sign(coeffH(22)))
text(23,0,'NbTiCr','FontSize',10,'Rotation',-90*sign(coeffH(23)))
text(24,0,'NbMoCr','FontSize',10,'Rotation',-90*sign(coeffH(24)))
text(25,0,'TiMoCr','FontSize',10,'Rotation',-90*sign(coeffH(25)))
text(26,0,'ZrNbTiMo','FontSize',10,'Rotation',-90*sign(coeffH(26)))
text(27,0,'ZrNbTiCr','FontSize',10,'Rotation',-90*sign(coeffH(27)))
text(28,0,'ZrNbMoCr','FontSize',10,'Rotation',-90*sign(coeffH(28)))
text(29,0,'ZrTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH(29)))
text(30,0,'NbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH(30)))
text(31,0,'ZrNbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH(31)))
hold off


figure;
hold on
bar (coeffH_raffined,'y')
text(1,0,'Zr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(1)))
text(2,0,'Nb','FontSize',10,'Rotation',-90*sign(coeffH_raffined(2)))
text(3,0,'Mo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(3)))
text(4,0,'Ti','FontSize',10,'Rotation',-90*sign(coeffH_raffined(4)))
text(5,0,'Cr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(5)))
text(6,0,'ZrNb','FontSize',10,'Rotation',-90*sign(coeffH_raffined(6)))
text(7,0,'ZrMo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(7)))
text(8,0,'ZrTi','FontSize',10,'Rotation',-90*sign(coeffH_raffined(8)))
text(9,0,'ZrCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(9)))
text(10,0,'NbMo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(10)))
text(11,0,'NbTi','FontSize',10,'Rotation',-90*sign(coeffH_raffined(11)))
text(12,0,'NbCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(12)))
text(13,0,'MoTi','FontSize',10,'Rotation',-90*sign(coeffH_raffined(13)))
text(14,0,'MoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(14)))
text(15,0,'TiCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(15)))
text(16,0,'ZrNbMo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(16)))
text(17,0,'ZrNbTi','FontSize',10,'Rotation',-90*sign(coeffH_raffined(17)))
text(18,0,'ZrNbCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(18)))
text(19,0,'ZrMoTi','FontSize',10,'Rotation',-90*sign(coeffH_raffined(19)))
text(20,0,'ZrMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(20)))
text(21,0,'ZrTiCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(21)))
text(22,0,'NbTiMo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(22)))
text(23,0,'NbTiCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(23)))
text(24,0,'NbMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(24)))
text(25,0,'TiMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(25)))
text(26,0,'ZrNbTiMo','FontSize',10,'Rotation',-90*sign(coeffH_raffined(26)))
text(27,0,'ZrNbTiCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(27)))
text(28,0,'ZrNbMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(28)))
text(29,0,'ZrTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(29)))
text(30,0,'NbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(30)))
text(31,0,'ZrNbTiMoCr','FontSize',10,'Rotation',-90*sign(coeffH_raffined(31)))
hold off



